local helper = {}

function helper.extend(baseObj, extended)
    if extended == nil then
        return helper.copy(baseObj)
    else
        for key, value in pairs(baseObj) do
            extended[key] = value
        end
    end
end

-- returns a copy of the object
function helper.copy(obj)
    local resultObj = {}
    for key, value in pairs(obj) do
        resultObj[key] = value
    end
    return resultObj
end

-- pushes a new object in a list
function helper.push(obj, value)
    if type(obj) == 'table' then
        obj[#obj+1] = value
    end
end

-- return the last value and delete it
function helper.pop(arr)
    local temp = arr[#arr]
    arr[#arr] = nil
    return temp
end

--- http://lua-users.org/wiki/TableSerialization
function helper.print_r (t, indent, done)
  done = done or {}
  indent = indent or ''
  local nextIndent -- Storage for next indentation value
  local result = ''
  for key, value in pairs (t) do
    if type (value) == "table" and not done [value] then
      nextIndent = nextIndent or
          (indent .. string.rep(' ',string.len(tostring (key))+2))
          -- Shortcut conditional allocation
      done [value] = true
      result = result .. (indent .. "[" .. tostring (key) .. "] => Table {") .. "\n";
      result = result .. (nextIndent .. "{") .. "\n";
      print_r (value, nextIndent .. string.rep(' ',2), done)
      result = result .. (nextIndent .. "}") .. "\n";
    else
      result = result .. (indent .. "[" .. tostring (key) .. "] => " .. tostring (value).."") .. "\n"
    end
  end
  return result
end

---------------
--- Aliases ---
---------------

helper.clone = helper.copy
helper.insert = helper.push

return helper