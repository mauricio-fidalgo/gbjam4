----------------------------------
--- Extendable object to pre 
--- allocate objects in memory.
--- 


local _ = require 'helpers/helpers'
local pool = {}
pool.pool = {}

-- initialize the pool
function pool:init(baseObject, numberToCache, canOverflow)
    if canOverflow == false then
        self.canOverflow = false
    else 
        self.canOverflow = true
    end
    self.baseObject = baseObject
    self.numberToCache = numberToCache
    self.baseObject.poolable = {}
    self:fill()
end

function pool:fill()
    for i = 1, self.numberToCache do
        self.pool[i] = _.copy(self.baseObject)
        self.pool[i].poolable.active = false
    end
end

-- get Last Object from the pool and remove it from there
function pool:getNew()
    local newObject = self:getInactiveObject();
    if self.canOverflow and not newObject then
        return self:generateNewObject()
    else
        newObject.poolable.active = true
        return newObject
    end
end

function pool:getInactiveObject()
    local inactiveObject = nil
    for i = 1, #self.pool do
        if not self.pool[i].poolable.active then
            inactiveObject = self.pool[i]
            break
        end
    end
    return inactiveObject
end

function pool:generateNewObject()
    self.pool[#self.pool + 1] = _.copy(self.baseObject)
    return self.pool[#self.pool]
end

-- add back to the pool
function pool:dispose(obj)
    for i = 1, #self.pool do
        if self.pool[i] == obj then
            self.pool[i].active = false
        end
    end
    return nil
end
return pool