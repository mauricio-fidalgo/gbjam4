------------------------
--- Set Globals here ---
------------------------
assets = {}
assets.images = {}
assets.audio = {}

------------------------
--- Important locals ---
------------------------

local loader = {}
local imagesFolder = 'assets/images/'
local audioFolder = 'assets/audio/'

------------------------------
--- INSERT HERE ALL IMAGES ---
------------------------------

-- Images
local images = {}
images['tileset01'] = 'tileset01.png'

-- Audio
local audio = {}

--------------
--- Coding ---
--------------

function loader.loadAll()
    loader.loadImages()
    loader.loadAudio()
end

function loader.loadImages()
    for name, fileName in pairs(images) do
        loader.loadImageFile(name,fileName)
    end
end
function loader.loadAudio()
    for name, fileName in pairs(audio) do
        loader.loadAudioFile(name, fileName)
    end
end

function loader.loadImageFile(name, fileName)
    assets.images[name] = love.graphics.newImage(imagesFolder..fileName)
end
function loader.loadAudioFile(name, fileName)
    assets.audio[name] = love.graphics.newImage(audioFolder..fileName)
end

return loader