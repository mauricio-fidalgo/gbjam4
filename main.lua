--------------------
--- Locals
--------------------
local _ = require('helpers/helpers')
local assetsLoader = require('config/assetsLoader')
function love.load()
    assetsLoader.loadAll()
    initialMap = require('objects/maps/exampleMap01')
end
function love.update()
    -- update logic
    initialMap.drawable.x = initialMap.drawable.x + 1
    initialMap.drawable.y = initialMap.drawable.y + 1
end
function love.draw()
    initialMap:draw()
end