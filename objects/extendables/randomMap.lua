
local randomMap = {}

randomMap.minRoomSize = {4,4}

-------------------------
--- List of map types ---
-------------------------
if ENUMS == nil then
	ENUMS = {}
end
ENUMS.MAP_TYPES = {}
ENUMS.MAP_TYPES['ARENA_WITH_ROOMS'] = 0
function randomMap:init(width, height, type)
	self.width = width
	self.height = height
	self.type = type
	self:dig()
end

function randomMap:dig()
end

return randomMap