local randomMapTiles = {}

function randomMapTiles:init(floor, celling)
	self.floor = floor
	self.celling = celling
end

-- each character represents the directio
-- u : up
-- d : down
-- l : left
-- r : right
-- m : middle
function randomMapTiles:setWalls(ul, dl, um, dm, ur, dr)
	self.wall = {}
	self.wall['ul'] = ul
	self.wall['dl'] = dl
	self.wall['um'] = um
	self.wall['dm'] = dm
	self.wall['ur'] = ur
	self.wall['dr'] = dr
end