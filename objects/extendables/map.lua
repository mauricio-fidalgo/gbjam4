-- 2d array with string/numerical values
local map = {}

-- String to int map is a key value 
-- object with the representative character as key and 
-- a numeric value representing the tile type on tile set

function map:init(_2dArr, stringToIntMap)
	self.intMap = {}
	if stringToIntMap == nil then
		self.intMap = _2dArr
	else
		for y = 1, #_2dArr do
			self.intMap[y] = {}
			for x = 1, #_2dArr[y] do
				self.intMap[y][x] = stringToIntMap[_2dArr[y][x]] -- Converts string value to basic int value
			end
		end
	end
end

return map