--[[
    Generates the Quads of a tileset and 
    other stuff needed on tileset usage
--]]

local tileset = {}

function tileset:init(image, tileWidth, tileHeight)
    -- clean the object in case of pooling
    self.tiles = {}
    self.image = {}
    self.tileWidth = tileWidth
    self.tileHeight = tileHeight
    self.image.image = image
    self.image.height = image:getHeight()
    self.image.width = image:getWidth()
    self:makeQuads()
end

-- Cut the image and make the tiles
function tileset:makeQuads()
    local imageWidthInTiles = math.floor(self.image.width / self.tileWidth);
    local imageHeightInTiles = math.floor(self.image.height / self.tileHeight);
    for x = 0, imageWidthInTiles-1 do
        for y = 0, imageHeightInTiles-1 do
            self.tiles[#self.tiles +1] = love.graphics.newQuad(x * self.tileWidth, y * self.tileHeight, self.tileWidth, self.tileHeight, self.image.width, self.image.height)
        end
    end
end

function tileset:drawTile(tile,x,y, ...)
    love.graphics.draw(self.image.image,self.tiles[tile],x,y, ...)
end
function tileset:drawTileAtTileMap(tile,tileX,tileY, ...)
    local x = tileX * self.tileWidth
    local y = tileY * self.tileHeight
    self:drawTile(tile, x, y, ...)
end

return tileset