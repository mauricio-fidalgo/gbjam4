local _ = require('helpers/helpers')
local tileMap = {}

function tileMap:init(tileset, map)
    self.drawable = _.clone(require('objects/extendables/drawable'))
    self.drawable:init()
    self.camera = _.clone(require('objects/extendables/rectangle'))
    self.tileset = tileset
    self.map = map
    self:createCanvas()
    self:preRender()
end

function tileMap:createCanvas()
    local width = self.tileset.tileWidth * #self.map.intMap[1]
    local height = self.tileset.tileHeight * #self.map.intMap
    self.canvas = love.graphics.newCanvas(width, height)
end

function tileMap:preRender()
    love.graphics.setCanvas(self.canvas)
    for i = 1, #self.map.intMap do
        for j = 1, #self.map.intMap[i] do
            self.tileset:drawTileAtTileMap(self.map.intMap[i][j], i -1, j-1)
        end
    end
    love.graphics.setCanvas()
end

function tileMap:draw()
    love.graphics.draw(self.canvas, self.drawable:prepareDraw())
end

return tileMap