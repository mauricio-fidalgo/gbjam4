local drawable = {}

function drawable:init(x,y,r,sx,sy,sx,ox,oy,kx,ky)
    self.x = x or 0
    self.y = y or 0
    self.r = r
    self.sx = sx
    self.sy = sy
    self.ox = ox
    self.oy = oy
    self.kx = kx
    self.ky = ky
end

function drawable:prepareDraw()
    return self.x, self.y, self.r, self.sx, self.sy, self.ox, self.oy, self.kx, self.ky
end

return drawable