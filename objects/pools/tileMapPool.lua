local _ = require('helpers/helpers')

local tileMapPool = _.clone(require('helpers/pool'))

tileMapPool:init(require('objects/extendables/tileMap'), 20, true)

return tileMapPool