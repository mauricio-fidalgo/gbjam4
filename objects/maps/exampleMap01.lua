local _ = require('helpers/helpers')
local tileMapPool = require('objects/pools/tileMapPool')
local stringMap = {
    {'#','#','#','#','#','#'},
    {'#','.','.','.','.','#'},
    {'#','.','.','.','.','#'},
    {'#','.','.','.','.','#'},
    {'#','.','.','.','.','#'},
    {'#','#','#','#','#','#'}
}
local stringToIntMap = {}
stringToIntMap['#'] = 1
stringToIntMap['.'] = 2

local resultMap = _.clone(require('objects/extendables/map'))

resultMap:init(stringMap, stringToIntMap)

local resultTileMap = tileMapPool:getNew()

local tileset = require('objects/tilesets/tileset01')

resultTileMap:init(tileset,resultMap)

return resultTileMap