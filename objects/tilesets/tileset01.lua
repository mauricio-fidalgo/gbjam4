local _ = require('helpers/helpers')
local resultTileset = _.clone(require('objects/extendables/tileset'))

resultTileset:init(assets.images['tileset01'],8,8)

return resultTileset